### 更新日志

2.16更新



[转到更新.md](%E6%9B%B4%E6%96%B0.md)


# 应用
- TROJAN
 * Trojan-Qt5
 链接：https://cloud.disroot.org/s/kmcoQTeNeeoRWLa
 * v2rayN-win-with-trojan
 链接：https://cloud.disroot.org/s/btEKBoMMYbPDqGK


# 代理
- V2ray
  * [01](https://cdn.jsdelivr.net/gh/air2006/V2RAY-SS/01DY.txt) jsdelivr加速（适中）
  * [01](https://git.disroot.org/bigword0529/proxy/raw/branch/master/01DY.txt) disroot同步仓库（最慢）
  * [01](https://github.daili001.workers.dev/air2006/proxy/raw/master/01DY.txt) Github镜像站（最快）
  > 三个链接内容一样，不同仓库
- TG代理
> [TG代理](\TG%E4%BB%A3%E7%90%86.txt)



## CDN加速
https://cdn.jsdelivr.net
## Github镜像站

https://github.daili001.workers.dev

